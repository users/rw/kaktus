BASE="$(pwd)"

export TEMPLATECONF="$(pwd)/meta-kaktus/conf/"

mkdir -p ${BASE}/downloads
mkdir -p ${BASE}/sstate

. poky/oe-init-build-env builddir

cat << EOF > conf/auto.conf
MACHINE = "raspberrypi3-64"
DISTRO = "kaktus"
KERNEL_MODULE_AUTOLOAD += " brcmfmac"
DL_DIR = "${BASE}/downloads"
SSTATE_DIR = "${BASE}/sstate"
CMDLINE_SERIAL = "console=serial0,115200"
RPI_EXTRA_CONFIG = "dtoverlay=disable-bt\n"
#BB_NO_NETWORK = "1"
EOF
